# Makefile for ebuild-run-mode Elisp package

# Author: Dima Akater

ifndef SITELISP
export SITELISP = /usr/share/emacs/site-lisp
else
export SITELISP
endif

UMAKEDEPS = org
UMAKEDEPS:=$(UMAKEDEPS:%=-L $(SITELISP)/%)

BUILDDEPS =
BUILDDEPS:=$(BUILDDEPS:%=-L $(SITELISP)/%)

EMACS_INIT 	:= emacs -Q --batch -L umake --load init.el

EMACS_UMAKE 	:= $(EMACS_INIT) $(UMAKEDEPS) --load umake.el

EVAL		:= $(EMACS_UMAKE) $(BUILDDEPS) -L . --eval

.PHONY: default all install clean uninstall

default:
	$(EVAL) "(umake 'default)"

all:
	$(EVAL) "(umake 'all)"

install:
	$(EMACS_INIT) --load install.el

clean:
	rm -rf *.el{,c} site-gentoo.d
# umake 'clean would work too but this is faster,
# and clean is run often

uninstall:
	rm -vf ${LISPDIR}*.el
	rm -vf ${LISPDIR}*.elc
# todo: umake 'uninstall
