;; -*- lexical-binding: t -*-

(defconst-with-prefix umake
  feature 'ebuild-run-mode
  authors "Gentoo Authors"
  first-publication-year-as-string "2020"
  source-files-in-order '("ebuild-run-mode")
  site-lisp-config-prefix "50")
