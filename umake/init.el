;;; -*- lexical-binding: t; -*-

(defun file-directory-p+ (filename)
  "When (FILE-DIRECTORY-P FILENAME) is non-nil, return FILENAME."
  (when (file-directory-p filename) filename))

(defun stringp+ (object)
  "When (STRINGP OBJECT) is non-nil, return OBJECT."
  (when (stringp object) object))

(defun read-list (string-or-nil)
  (when string-or-nil
    (with-temp-buffer
      (insert ?\( string-or-nil ?\))
      (goto-char (point-min))
      (read (current-buffer)))))

(require 'cl-macs)

(when (< emacs-major-version 28)
  (cl-deftype keyword () `(satisfies keywordp)))

(defun ensure-string (x)
  (cl-etypecase x
    (string x)
    (keyword (substring (symbol-name x) 1))
    (symbol (symbol-name x))
    (number (number-to-string x))))

(require 'cl-seq)

(defun fold (f x list) (cl-reduce f list :initial-value x))

(defmacro fif (test f x)
  "If TEST evaluates to non-nil, return (F X).  Otherwise, return X."
  (declare (indent 1))
  `(if ,test (funcall ,f ,x) ,x))

(cl-defmacro do-default-directory-files ((var match
                                              &optional full nosort return)
                                         &body body)
  "If BODY starts with a string, it is presumed to be format strings with two %s slots: length, plural/singular -s"
  (declare (indent 1))
  (let* ((files-g (gensym "files-"))
         (format-string (stringp+ (car body)))
         (count-g (when format-string (gensym "count-")))
         (do-not-count-g (when format-string (gensym "do-not-count-")))
         (body (fif format-string #'cdr body)))
    `(let ((,files-g (directory-files default-directory
                                      ,full ,match ,nosort)))
       ,(fif format-string (lambda (form)
                             `(let ((,count-g 0))
                                (prog1 ,form
                                  (message ,format-string
                                           ,count-g
                                           (if (= 1 ,count-g) "" "s")))))
             `(dolist (,var ,files-g ,return)
                ,@(if format-string
                      `((let (,do-not-count-g)
                          (cl-flet ((do-not-count ()
                                                  (setf ,do-not-count-g t)))
                            ,@body)
                          (unless ,do-not-count-g (cl-incf ,count-g))))
                    body))))))

(defun ensure-directory (dir) (make-directory dir t) dir)

(cl-defmacro with-filenames-relative-to (root names &body body)
  (declare (indent 2))
  (let ((root-g (gensym "root-")))
    `(let ((,root-g ,root))
       (cl-flet ((file-name-relative
                  (filename)
                  (expand-file-name (replace-regexp-in-string
                                     (rx string-start (one-or-more ?/)) ""
                                     filename)
                                    ,root-g)))
         (let ,(mapcar (lambda (x) `(,x (when ,x (file-name-relative ,x))))
                       names)
           ,@body)))))

(defmacro defconst-with-prefix (prefix &rest pairs)
  (declare (indent 1))
  (let ((prefix-name (symbol-name prefix)))
    `(cl-eval-when (:compile-toplevel :load-toplevel :execute)
       ,@(cl-loop for (symbol value) on pairs by #'cddr
                  collect `(defconst ,(intern (concat prefix-name "-"
                                                      (symbol-name symbol)))
                             ,value)))))
