;; -*- lexical-binding: t -*-

(load "init" nil t)
(load "umake-buildenv" nil t)

(defconst root (or (getenv "DESTDIR") "/"))

(with-filenames-relative-to root ( sitelisp lispdir
                                   org-immutable-sources-directory)

  (when (and site-autoloads (file-directory-p "site-gentoo.d"))
    (let ((default-directory (expand-file-name "site-gentoo.d")))
      (do-default-directory-files (file (rx ".el" string-end) nil t)
        (rename-file file (concat umake-site-lisp-config-prefix file))))
    (copy-directory "site-gentoo.d"
                    (expand-file-name "site-gentoo.d" sitelisp)
                    t t t)
    (message "Installed site-lisp configuration"))

  (ensure-directory lispdir)
  (do-default-directory-files (el-or-elc-file
                               (rx ".el" (zero-or-one ?c) string-end)
                               nil t)
    "Installed %s source (el) or byte-compiled (elc) file%s"
    (when (ignore-errors (string-equal autoloads-file el-or-elc-file))
      (message "Installing autoloads file in-source..."))
    (copy-file el-or-elc-file lispdir))

  (when org-immutable-sources-directory
    (ensure-directory org-immutable-sources-directory)
    (do-default-directory-files (linked-org-file
                                 (rx ".org" string-end) nil t)
      "Installed %s linked Org file%s"
      (if (file-symlink-p linked-org-file) (do-not-count)
        (copy-file linked-org-file (file-name-as-directory
                                    org-immutable-sources-directory))))))
