;;; -*- lexical-binding: t; -*-

(load "umake-buildenv" nil t)

(defconst org-sources-directory (or org-local-sources-directory
                                    org-immutable-sources-directory))

(defun umake-message (format-string &rest args)
  (apply #'message (concat "%s: " format-string)
         'umake args))

(when org-local-sources-directory
    (unless (stringp org-local-sources-directory)
      (error "Error: %s is not a string: %s"
             'org-local-sources-directory org-local-sources-directory))
    (unless (string-prefix-p "/" org-local-sources-directory)
      (error "Error: Local org sources directory is ambiguous: %s"
             org-local-sources-directory))
    (umake-message "%s is set to %s"
                    'org-local-sources-directory
                    org-local-sources-directory)
    (umake-message "Relative links in el source will be overridden"))

;; don't litter (does it litter at all in batch mode?)
(setq backup-by-copying t
      backup-directory-alist `(("." . ,(expand-file-name
                                        ".saves" user-emacs-directory)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control nil)

(require 'ob-tangle)

;; this is likely only needed when org had been ripped off Emacs
;; and installed separately
(require 'org)

(require 'org-element)
(setq org-element-cache-persistent nil)

;; silence some messages
(defalias 'org-development-elisp-mode 'org-mode)

(defvar ob-flags use-flags)

(require 'ol)
(defun org--akater-redirect-link (link directory)
  (if (and (string-match org-link-types-re link)
	   (string= (match-string 1 link) "file"))
      (concat "file:"
	      (expand-file-name
               ;; basically exactly how it's done in ob-tangle
               (substring link (match-end 0))
	       directory))
    link))

(defmacro with-org-links-redirection-to (directory &rest body)
  (declare (indent 1))
  (let ((spec (gensym "spec-"))
        (directory-g (gensym "org-links-redirection-directory-")))
    `(let ((,directory-g ,directory))
       (cl-check-type ,directory-g (or null string))
       (if ,directory-g
           (cl-flet ((override-link-in-spec (,spec)
                                            (setf (caddr ,spec)
                                                  (org--akater-redirect-link
                                                   (caddr ,spec)
                                                   ,directory-g))
                                            ,spec))
             (advice-add 'org-babel-tangle-single-block
                         :filter-return #'override-link-in-spec)
             (unwind-protect (let ((org-babel-default-header-args
                                    (cons '(:comments . "link")
                                          org-babel-default-header-args)))
                               ,@body)
               (advice-remove 'org-babel-tangle-single-block
                              #'override-link-in-spec)))
         ,@body))))

(defun insert-gpl-license
    (one-line-to-give-the-programs-name-and-an-idea-of-what-it-does
     initial-year-as-string authors &optional buffer)
  (setq buffer (or buffer (current-buffer)))
  (with-current-buffer (find-file-noselect "COPYING")
    (goto-char (point-min))
    (re-search-forward
     (rx line-start "How to Apply These Terms to Your New Programs"))
    (re-search-forward
     (rx line-start
         "one line to give the program's name and an idea of what it does."))
    (let ((start (with-current-buffer buffer (point))))
      (with-current-buffer buffer
        (insert
         one-line-to-give-the-programs-name-and-an-idea-of-what-it-does ?\n))
      (re-search-forward (rx line-start))
      (let ((copyright-pattern
             (if (not (looking-at-p "Copyright"))
                 (error "Can't find copyright line in the template")
               (buffer-substring-no-properties
                (point) (progn (end-of-line) (point)))))
            (years (let ((current-year (format-time-string "%Y")))
                     (if (string-equal initial-year-as-string current-year)
                         current-year
                       (concat initial-year-as-string "--" current-year)))))
        (with-current-buffer buffer
          (insert
           (fold (lambda (string rule)
                   (replace-regexp-in-string (car rule) (cdr rule) string))
                 copyright-pattern
                 `((,(rx "yyyy") . ,years)
                   (,(rx "name of author") . ,authors))))))
      (let ((rest-of-the-license (buffer-substring-no-properties
                                  (point)
                                  (progn (re-search-forward
                                          (rx line-start "Also add"))
                                         (beginning-of-line) (point)))))
        (with-current-buffer buffer
          (insert rest-of-the-license)
          (comment-region start (point)))))
    (kill-buffer)))

(defun insert-newline-to-please-unix () (insert ?\n))

(defun insert-elisp-postamble (file)
  (with-current-buffer (find-file-noselect file)
    (goto-char (point-max))
    (insert ?\n)
    (prin1 `(provide ',(intern (file-name-base file))) (current-buffer))
    (insert ?\n ?\n
            (file-name-nondirectory file) " ends here")
    (comment-region (line-beginning-position) (line-end-position))
    (insert-newline-to-please-unix)
    (save-buffer) (kill-buffer))
  file)

(defun umake-prepare ()
  ;; Here, we used to
  ;; cl-letf (((symbol-function 'delete-file) #'ignore))
  ;; This is a hack described in https://emacs.stackexchange.com/a/38898
  ;; to allow tangling from multiple org files into a single file.
  ;; Looks like it does not even work with contemporary org-mode;
  ;; I'm working on a much more thorough solution now.
  (let ((org-babel-tangle-use-relative-file-links t))
    (with-org-links-redirection-to org-sources-directory
      ;; this is a hack too
      ;; supposedly, we better would be able to specify
      ;; `org-babel-tangle-use-relative-file-links' as string,
      ;; and that string would be used as DIRECTORY argument
      ;; in `file-relative-name', in `org-babel-tangle-single-block'
      ;; any non-string might be treated
      ;; equivalently to default-directory (dynamically looked up)
      ;; btw, why is the return value of org-babel-tangle-single-block
      ;; uses (file-relative-name file) for file name
      ;; but (file-relative-name file #<something-non-nil>) for the link?
      ;; this is inconsistent
      (let (all-tangled-source-files)
        (dolist (file umake-source-files-in-order
                      (mapcar #'insert-elisp-postamble
                              all-tangled-source-files))
          (let ((org-file (concat file ".org")))
            (dolist (tangled-file (org-babel-tangle-file org-file))
              (unless (or (cl-member tangled-file all-tangled-source-files
                                     :test #'string-equal)
                          (string-equal "site-gentoo.d"
                                        ;; not a source file
                                        ;; but we need a better check
                                        (file-name-nondirectory
                                         (directory-file-name
                                          (file-name-directory
                                           tangled-file)))))
                (let ((name (file-name-nondirectory tangled-file))
                      (description
                       (with-current-buffer (find-file-noselect org-file)
                         (when (re-search-forward
                                (rx line-start "#+description: ")
                                nil t)
                           (buffer-substring-no-properties
                            (point) (line-end-position)))))
                      (local-variables '(:lexical-binding t)))
                  (with-current-buffer (find-file-noselect tangled-file)
                    (goto-char (point-min))
                    (insert-gpl-license
                     (with-temp-buffer
                       (insert name)
                       (when description (insert " --- " description))
                       (when local-variables
                         (insert "  " "-*- ")
                         (cl-loop
                          for (key value) on local-variables by #'cddr
                          do (insert (ensure-string key) ": "
                                     (ensure-string value)))
                         (insert " -*-"))
                       (buffer-string))
                     umake-first-publication-year-as-string
                     umake-authors)
                    (save-buffer)))
                (push tangled-file all-tangled-source-files))))))))
  'prepared)

(defun umake-config ()
  ;; when site-lisp-configuration is absent,
  ;; any config beyond in-source autoloads is discarded
  (when autoloads-file
    ;; I generally do not `require' in defuns
    ;; but this file is one-time init
    (require 'autoload)
    (let ((generated-autoload-file (expand-file-name
                                    (if site-autoloads
                                        (format "site-gentoo.d/%s-gentoo.el"
                                                umake-feature)
                                      autoloads-file))))
      (with-current-buffer (find-file-noselect generated-autoload-file)
        (ensure-directory default-directory)
        (save-buffer)                   ; file should exist
        (if (not site-autoloads) (progn (kill-buffer)
                                        (update-directory-autoloads
                                         default-directory))
          (let ((inhibit-read-only t))
            (goto-char (point-min))
            (insert ?\n
                    (format ";;; %s site-lisp configuration"
                            umake-feature)
                    ?\n ?\n)
            (prin1 `(add-to-list 'load-path ,(expand-file-name
                                              (ensure-string umake-feature)
                                              "/usr/share/emacs/site-lisp/"))
                   (current-buffer))
            (insert ?\n ?\n)
            (goto-char (point-max))
            (insert ?\n
                    (format ";;; begin: forms written by `%s'"
                            'autoload-generate-file-autoloads)
                    ?\n)
            (save-buffer)
            (let ((default-directory (expand-file-name ".." default-directory)))
              (do-default-directory-files (el-file (rx ".el" string-end) t t)
                ;; todo: deal with the case when el-file
                ;; specifies generated-autoload-file
                (insert ?\n)
                (let ((generated-autoload-load-name (file-name-base el-file)))
                  (autoload-generate-file-autoloads el-file (current-buffer)))))
            (insert ?\n
                    (format ";;; end: forms written by `%s'"
                            'autoload-generate-file-autoloads)
                    ?\n))
          (save-buffer) (kill-buffer)))))
  'autoloads)

(defvar umake-compile-error nil)

(defun umake-compile ()
  (do-default-directory-files (el-file (rx ".el" string-end) t
                                       t ;; order of compilation is arbitrary
                                       ;; according to
                                       ;;   File: elisp.info
                                       ;;   Node: Multi-file Packages
                                       )
    "Compiled %s file%s"
    (if (string-equal "sitefile" (file-name-base el-file)) (do-not-count)
      (unless (byte-compile-file el-file)
        (do-not-count)
        (setf umake-compile-error t))))
  (umake-config)
  (unless umake-compile-error
    'compiled))

(defun umake (target &optional live)
  "Kill Emacs in the end (with appropriate error code) when LIVE is non-nil."
  (when live
    (cl-assert target "Null target doesn't allow distinguishing errors and non-errors when LIVE is non-nil"))
  (let ((umake-compile-error umake-compile-error))
    (umake-message "making target %s" target)
    (cl-ecase target
      (all
       (umake 'default t))
      (clean (do-default-directory-files (el-or-elc-file
                                          (rx ".el" (zero-or-one ?c) string-end)
                                          nil t)
               "Deleted %s source (el) or byte-compiled (elc) file%s"
               (delete-file el-or-elc-file))
             (delete-directory "site-gentoo.d" t))
      (default
        (umake-prepare)
        (umake-compile)))
    (if live
        (unless umake-compile-error
          (umake-message "made target %s" target)
          target)
      ;; we return 1 for compile-error
      ;; because that's what batch-byte-compile-file returns
      (kill-emacs (if umake-compile-error 1 0)))))

